# }bio{borgs

Este repositório contém o código-fonte TidalCycles, SuperCollider e samples
utilizados para produzir os sons e músicas do álbum }bio{borgs em colaboração
com Biophillick e euFraktus.

Mais informações e acesso ao álbum em:
* https://biophillick.com/BIOBORGS
* http://joenio.me/bioborgs
